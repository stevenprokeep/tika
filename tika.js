// requires a tika server: 
// docker run -d -p 9998:9998 apache/tika

const fs = require("fs");
const path = require("path");
const argv = require('minimist')(process.argv.slice(2));
const axios = require('axios');
const readDir = argv['readDir'];
const writeDir = argv['writeDir'];
const httpAdapter = require('axios/lib/adapters/http');

const tikaURI = `http://localhost:9998/tika`;
fs.rmdirSync(writeDir, { recursive: true });
fs.mkdirSync(writeDir);

async function* walk(dir) {
    for await (const d of await fs.promises.opendir(dir)) {
        const entry = path.join(dir, d.name);
        if (d.isDirectory()) yield* walk(entry);
        else if (d.isFile()) yield entry;
    }
}

(async () => {
    for await (const p of walk(readDir))
        if (p){
            const pathArr = path.parse(p);
            if (pathArr['ext'] === '.docx') {

            console.log(`${p} -> ${writeDir}/${pathArr['base']}`)

            inFile = fs.createReadStream(p); 

            const outfile = `${writeDir}/${pathArr['base'].replace('docx','txt')}`

            const output = fs.createWriteStream(outfile);
            //request the file
            axios({
              method: 'PUT',
              url: tikaURI,
              data: inFile,
              headers: {'Content-Type':'application/vnd.openxmlformats-officedocument.wordprocessingml.document'},
              options: {responseType: 'stream', adapter: httpAdapter}
            })
            .then((response) => {
                console.log(response.data);
                output.write(response.data);
            })
            .catch((err) =>{
              console.error(err);
              process.exit()
            })
            }
        } 
})()